//
//  CaptuewViewController.swift
//  
//
//  Created by Phatthanaphong on 23/5/2561 BE.
//

import UIKit
import AVFoundation

class CaptureViewController: UIViewController {
    
    @IBOutlet weak var preview: UIView!
    
    var captureSession = AVCaptureSession()
    var backCamera: AVCaptureDevice?
    var frontCamera: AVCaptureDevice?
    var currentCamera: AVCaptureDevice?
    var photoOutput: AVCapturePhotoOutput?
    var cameraPrevireLayer : AVCaptureVideoPreviewLayer?
    var image : UIImage?
    var textID = String()  // text id passed from the QR
    
    @IBOutlet weak var btn: UIButton!
    @IBOutlet weak var inBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        btn.layer.shadowColor = UIColor.black.cgColor
        btn.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        btn.layer.masksToBounds = false
        btn.layer.shadowRadius = 1.0
        btn.layer.shadowOpacity = 0.5
        btn.layer.cornerRadius = btn.frame.width / 2
        
        inBtn.layer.shadowColor = UIColor.black.cgColor
        inBtn.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        inBtn.layer.masksToBounds = false
        inBtn.layer.shadowRadius = 1.0
        inBtn.layer.shadowOpacity = 0.5
        inBtn.layer.cornerRadius = inBtn.frame.width / 2
        
        setupCaptureSession()
        setupDevice()
        setupInputOutput()
        setupPreviewLayer()
        startRunninCaptureSession()

        // Do any additional setup after loading the view.
    }

    func setupCaptureSession(){
        // to get the full resolution
        captureSession.sessionPreset = AVCaptureSession.Preset.photo;
        
    }
    func setupDevice(){
        let deviceDiscoverySession =  AVCaptureDevice.DiscoverySession(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera], mediaType: AVMediaType.video, position: AVCaptureDevice.Position.unspecified)
        let devices = deviceDiscoverySession.devices
        
        
        for device in devices{
            if device.position == AVCaptureDevice.Position.back{
                backCamera = device
                
            }
            else if device.position == AVCaptureDevice.Position.front{
                frontCamera = device
                
            }
        }
        currentCamera = backCamera
        
    }
    
    func setupInputOutput(){
        do{
            let captureDeviceInput = try AVCaptureDeviceInput(device : currentCamera!)
            captureSession.addInput(captureDeviceInput)
            photoOutput = AVCapturePhotoOutput()
            photoOutput?.setPreparedPhotoSettingsArray([AVCapturePhotoSettings(format:[AVVideoCodecKey:AVVideoCodecType.jpeg])], completionHandler: nil)
            captureSession.addOutput(photoOutput!)
        }catch{
            print(error)
        }
    }
    
    func setupPreviewLayer(){
        cameraPrevireLayer = AVCaptureVideoPreviewLayer(session : captureSession)
        cameraPrevireLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        cameraPrevireLayer?.connection?.videoOrientation = AVCaptureVideoOrientation.landscapeRight
        cameraPrevireLayer?.frame = preview.bounds
        //cameraPrevireLayer?.f
        preview.layer.addSublayer(cameraPrevireLayer!)
        
        //drawing a red rect to guide users
        let rect = CAShapeLayer()
        rect.path = UIBezierPath(rect: CGRect(x: 3, y: 0, width: preview.frame.width-3, height: 60)).cgPath

        rect.position = CGPoint(x: 0, y: preview.frame.height/2-30)
        rect.fillColor = nil
        rect.strokeColor = UIColor.green.cgColor
        
        preview.layer.addSublayer(rect)
        
        
        //rect.path = UIBezierPath(
        var fontName: CFString = "Noteworthy-Light" as CFString
        //noteworthyLightFont = CTFontCreateWithName(fontName, baseFontSize, nil)
        fontName = "Helvetica" as CFString
        //helveticaFont = CTFontCreateWithName(fontName, baseFontSize, nil)
        /*
        let textLayer = CATextLayer()
        textLayer.frame = preview.bounds
        let string = String("  {                                                              }")
        textLayer.string = string
        textLayer.font = CTFontCreateWithName(fontName, 24, nil)
        textLayer.foregroundColor = UIColor.green.cgColor
        textLayer.isWrapped = true
        textLayer.alignmentMode = kCAAlignmentLeft
        textLayer.contentsScale = UIScreen.main.scale
        textLayer.position = CGPoint(x:320, y:250)
        preview.layer.addSublayer(textLayer)
         */
        view.layer.insertSublayer(preview.layer, above: view.layer)
    }
    
    func startRunninCaptureSession(){
        captureSession.startRunning()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backToPrev(_ sender: Any) {
       //navigationController?.popViewController(animated: true)
        self.dismiss(animated: true)
    }
    
    @IBAction func captureBtn(_ sender: Any) {
        let settings = AVCapturePhotoSettings()
        photoOutput?.capturePhoto(with: settings, delegate: self)
    }
    @IBAction func captuureImage(_ sender: Any) {
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showPhoto_segue"{
            let previewIm = segue.destination as! VideoViewController
            previewIm.image = self.image
            previewIm.textID = self.textID
        }
    }
}

extension CaptureViewController:AVCapturePhotoCaptureDelegate{
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        if let imageData = photo.fileDataRepresentation(){
            image = UIImage(data: imageData)
            performSegue(withIdentifier: "showPhoto_segue", sender: nil)
        }
    }
}
