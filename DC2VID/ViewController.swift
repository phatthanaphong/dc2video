//
//  ViewController.swift
//  DC2VID
//
//  Created by Phatthanaphong on 22/5/2561 BE.
//  Copyright © 2561 Phatthanaphong. All rights reserved.
//

import UIKit
import AVFoundation
import SwiftyPlistManager
//import QRCodeReader


class ViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate{
    
    @IBOutlet weak var camArea: UIView!
    
    @IBOutlet var videoPreview: UIView!

    var audioPlayer = AVAudioPlayer()
    
    var initURLText = String()
    
    // define the host!
    var staticHost = "http://202.28.34.202"
    
    var textID = String()
    enum error : Error{
        case noCameraAvaialable
        case videoInputInitialFail
    }
   
    override func viewDidLoad() {
        super.viewDidLoad()
        checkPlist()
        do{
            try scanQRCode()
            //print("nothing")
        }
        catch{
            print("Fail to scan the QR")
        }
    }
    
    // entry point
    func checkPlist(){
    
        let url = URL(string: staticHost+"/TextPrepPilot/config.txt")!
        let task = URLSession.shared.dataTask(with:url) { (data, response, error) in
            if error != nil {
                print(error!)
            }
            else {
                if let textFile = String(data: data!, encoding: .utf8) {
                    let text = textFile
                    let textArr = text.components(separatedBy: "\n");
                    self.initURLText = textArr[0]
                    //print(self.initURLText)
                    //self.initURLText = "http://10.33.14.44"

                }
            
                guard let fetchedValue = SwiftyPlistManager.shared.fetchValue(for: "initURL", fromPlistWithName: "Data")
                    else {
                        //load the data from the server and write it to the data plist
                        //write to the plist
                        SwiftyPlistManager.shared.addNew(self.initURLText, key:"initURL" , toPlistWithName: "Data") { (err) in
                            if err == nil {
                                print("Value successfully added into plist.")
                            }
                        }
                        
                        return
                };
                
                guard let initText  = SwiftyPlistManager.shared.fetchValue(for: "initURL", fromPlistWithName: "Data") else { return }
               
            
            
                if(self.initURLText.compare(initText as! String).rawValue != 0){
                    
                    SwiftyPlistManager.shared.save(self.initURLText, forKey: "initURL", toPlistWithName: "Data") { (err) in
                        if err == nil {
                            print("Value successfully saved into plist.")
                        }
                    }
                }
            }
        }
        
        task.resume()
    }
    
    func  metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection){
       
        if metadataObjects.count > 0 {
            let machineReableCode = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
            if machineReableCode.type == AVMetadataObject.ObjectType.qr{
                 textID = machineReableCode.stringValue!
                
                 performSegue(withIdentifier: "do_capture", sender: nil)
            }
        }
    }
    
    func scanQRCode() throws{
        let avCaptureSession = AVCaptureSession()
        guard let avCaptureDevice = AVCaptureDevice.default(for: AVMediaType.video) else {
            print("no camera avialble")
            throw error.noCameraAvaialable
        }
        
        guard let avCaptureInput =  try? AVCaptureDeviceInput(device: avCaptureDevice) else{
            print("Failed to init camera")
            throw error.videoInputInitialFail
        }
        
        let avCaptureMetadataOutput = AVCaptureMetadataOutput()
        avCaptureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        avCaptureSession.addInput(avCaptureInput)
        avCaptureSession.addOutput(avCaptureMetadataOutput)
        
        avCaptureMetadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
        
        let avCaptureVideoPreiewLayer = AVCaptureVideoPreviewLayer(session: avCaptureSession)
        avCaptureVideoPreiewLayer.connection?.videoOrientation = AVCaptureVideoOrientation.landscapeRight
        avCaptureVideoPreiewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        avCaptureVideoPreiewLayer.frame = camArea.bounds
       
        self.camArea.layer.addSublayer(avCaptureVideoPreiewLayer)
        
        let rect = CAShapeLayer()
        rect.path = UIBezierPath(rect: CGRect(x: 0, y: 0, width: 100, height: 100)).cgPath
        //print(self.view.frame.width)
        //print(self.view.frame.height)
        rect.position = CGPoint(x: 20 , y: 20)
        rect.fillColor = nil
        rect.strokeColor = UIColor.red.cgColor
                
        self.videoPreview.layer.insertSublayer(camArea.layer, above: self.videoPreview.layer)
        
        avCaptureSession.startRunning()
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "do_capture"{
            let captureView = segue.destination as! CaptureViewController
            captureView.textID = self.textID
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}



