//
//  OpenCVWrapper.h
//  DC2VID
//
//  Created by Phatthanaphong on 23/5/2561 BE.
//  Copyright © 2561 Phatthanaphong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef struct{
    int x1;
    int y1;
    int x2;
    int y2;
} line;

@interface OpenCVWrapper : NSObject
+(NSString *) openCVversionString;
+(UIImage *) nameGrayscale:(UIImage *) image;
+(UIImage *) imageResize:(UIImage *) image;
+(UIImage *) lineDetection:(UIImage *) image;
+(UIImage *) normalization:(UIImage *) image;
@end
